package wahidah.hildha.peminjamanbarangmahasiswa

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_update.*
import org.json.JSONException
import org.json.JSONObject
import java.util.HashMap

class updatelist : AppCompatActivity() {

    val i=50
    val VIEW =101
    var code:String?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_update)
        val b:Bundle= intent.extras!!
        code=b.getString("id_pinjam")
        tukode.text=code.toString()
        tunama.setText(b.getString("nama_pinjam"))
        tunomor.setText(b.getString("no_hp"))
        tutanggal.setText(b.getString("tgl_pinjam"))
        tubarang.setText(b.getString("kode_barang"))
        tustatus.setText(b.getString("status"))
       // Picasso.get().load("http://192.168.43.81/pinjambarang/image/"+b.getString("ktp")).resize(1000, 700).centerCrop().into(imvu)



    }

    fun update(view: android.view.View) {

        val lis= Response.Listener<String> { response ->
            try {
                val json= JSONObject(response)
                val success:Boolean=json.getBoolean("success")
                if (success){
                    var intent = Intent(this, listdatapeminjaman_activity::class.java)
                    startActivityForResult(intent,VIEW)
                    finish()
                }else {
                    Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show()
                }
            } catch (e: JSONException) {
                Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show()
            }
        }

        val send:Send=Send(code!!,lis)
        val request: RequestQueue = Volley.newRequestQueue(this)
        request.add(send)
    }

    class Send : StringRequest {
        companion object{
            private val Url= "http://192.168.43.81/pinjambarang/update.php"

        }
        private var map:MutableMap<String,String> = HashMap()
        constructor(code:String,Listener: Response.Listener<String>)
                :super(Request.Method.POST,Url,Listener,null){
            map.put("id_pinjam",code)
        }
        override fun getParams(): MutableMap<String, String> {
            return map
        }
    }
}
