package wahidah.hildha.peminjamanbarangmahasiswa

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.itempeminjaman.view.*
import kotlinx.android.synthetic.main.list_daftar_peminjaman.*
import org.json.JSONException
import org.json.JSONObject

class listdatapeminjaman_activity : AppCompatActivity() {

    var vol: RequestQueue?=null
    private val url= "http://192.168.43.81/pinjambarang/view.php"
    var arr=ArrayList<Database>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.list_daftar_peminjaman)

        vol= Volley.newRequestQueue(this)
        val json= JsonObjectRequest(Request.Method.GET,url, Response.Listener <JSONObject>{
                response ->
            try {
                val j=response.getJSONArray("alldata")
                for (i in 0 until j.length())
                {
                    val jes=j.getJSONObject(i)
                    val id_pinjam= jes.getString("id_pinjam")
                    val nama_pinjam= jes.getString("nama_pinjam")
                    val no_hp= jes.getString("no_hp")
                    val ktp = jes.getString("ktp")
                    val tgl_pinjam= jes.getString("tgl_pinjam")
                    val kode_barang= jes.getString("kode_barang")
                    val status= jes.getString("status")
                    arr.add(Database(id_pinjam,nama_pinjam,no_hp,ktp,tgl_pinjam,kode_barang,status))
                    val a=data(arr,this)
                    list_peminjaman.adapter=a
                }
                //Toast.makeText(this, "Done"+j, Toast.LENGTH_SHORT).show()
            }catch (e: JSONException)
            {
                e.printStackTrace();
            }
        }, Response.ErrorListener {
                error: VolleyError? -> Log.e("error", "error")
            Toast.makeText(this, "Sambungan Gagal", Toast.LENGTH_SHORT).show()
        }
        )
        vol!!.add(json)
    }
    class data : BaseAdapter {

        var adap=ArrayList<Database>()
        var context: Context?=null
        constructor(arr:ArrayList<Database>,con: Context)
        {
            adap=arr
            context=con
        }
        override fun getView(p0: Int, p1: View?, p2: ViewGroup?): View {
            val d=adap[p0]
            val inf=context!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val f=inf.inflate(R.layout.itempeminjaman,null)
            val id_pinjam=d.id_pinjam
            val nama_pinjam=d.nama_pinjam
            val no_hp=d.no_hp
           //Picasso.get().load("http://192.168.43.81/pinjambarang/image/"+d.ktp).resize(455, 304).centerCrop().into(f.imvd)
            val tgl_pinjam=d.tgl_pinjam
            val kode_barang=d.kode_barang
            val status=d.status
            f.kode.text=id_pinjam
            f.nama.text=nama_pinjam
            f.tgl.text=tgl_pinjam
            f.nohp.text=no_hp
            f.barang.text=kode_barang
            f.status.text=status

            //ketika image view ditekan akan pindah ke layout updatelist mengambil data arraylist database
            f.kode.setOnClickListener{
                val int= Intent(context!!,updatelist::class.java)
                int.putExtra("id_pinjam",d.id_pinjam)
                int.putExtra("nama_pinjam",d.nama_pinjam)
                int.putExtra("no_hp",d.no_hp)
               // int.putExtra("ktp",d.ktp)
                int.putExtra("tgl_pinjam",d.tgl_pinjam)
                int.putExtra("kode_barang",d.kode_barang)
                int.putExtra("status",d.status)
                context!!.startActivity(int)
            }
            return f
        }

        override fun getItem(p0: Int): Any {
            return adap[p0]
        }

        override fun getItemId(p0: Int): Long {
            return p0.toLong()
        }

        override fun getCount(): Int {
            return adap.size
        }
    }
}