package wahidah.hildha.peminjamanbarangmahasiswa

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.activity_menu.*

class menuActivity : AppCompatActivity(), View.OnClickListener {


    val pinjam : Int = 101
    val daftar : Int = 102


    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btn_peminjaman ->{
                var intent = Intent(this,MainActivity::class.java)
                startActivityForResult(intent,pinjam)
            }
            R.id.btn_daftarpinjam ->{
                var intent = Intent(this,listdatapeminjaman_activity::class.java)
                startActivityForResult(intent,daftar)
            }
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)
        btn_peminjaman.setOnClickListener(this)
        btn_daftarpinjam.setOnClickListener(this)
        setupPermissions()

    }
    private val WRITE_REQUEST_CODE = 101


    private fun setupPermissions() {
        val permission = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        if (permission != PackageManager.PERMISSION_GRANTED) {
            makeRequest()
        }
    }


    private fun makeRequest() {
        ActivityCompat.requestPermissions(this,
            arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), WRITE_REQUEST_CODE)
    }

}