package wahidah.hildha.peminjamanbarangmahasiswa

import android.Manifest
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.media.MediaScannerConnection
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Base64
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.zxing.integration.android.IntentIntegrator
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONException
import org.json.JSONObject
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.*

class MainActivity : AppCompatActivity(), View.OnClickListener {

    val VIEW =101
    private val CAMERA = 1
    private val GALERY = 2
    private val CAMERA_REQUEST_CODE=123;

    //membuat datepicker
    var tahun = 0
    var bulan = 0
    var hari = 0

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnscan -> {
                intentIntegrator.setBeepEnabled(true).initiateScan()
            }
            R.id.btntglmasuk -> showDialog( 20)
        }
    }

    lateinit var intentIntegrator: IntentIntegrator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //memanggil function kode acak random dengan length 6
        random(6)

        permissioncamera()

        //membuat datepicker
        val cal : Calendar = Calendar.getInstance()
        bulan = cal.get(Calendar.MONTH)
        hari = cal.get(Calendar.DAY_OF_MONTH)
        tahun = cal.get(Calendar.YEAR)
        btntglmasuk.setOnClickListener(this)
        intentIntegrator = IntentIntegrator(this)
        btnscan.setOnClickListener(this)
    }

    fun insert(view: View) {

        val lis= Response.Listener<String> { response ->
            try {
                val json= JSONObject(response)
                val success:Boolean=json.getBoolean("success")
                if (success){
                    Toast.makeText(this, "Peminjaman Berhasil", Toast.LENGTH_SHORT).show()
                    random(6)
                    txnamapinjam.setText("")
                    txnohp.setText("")
                    txtglpinjam.setText("")
                    txnamabarang.setText("")
                    txkodebarang.setText("")

                }else {
                    Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show()
                }
            } catch (e: JSONException) {
                Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show()
            }
        }

        val bit: Bitmap =((imvktp.drawable)as BitmapDrawable).bitmap
        var arr= ByteArrayOutputStream()
        bit.compress(Bitmap.CompressFormat.JPEG,100,arr)

        val img= Base64.encodeToString(arr.toByteArray(), Base64.DEFAULT)
        val send:Send=Send(id_pinjam.text.toString(),txnamapinjam.text.toString(),txnohp.text.toString(),img,txtglpinjam.text.toString(),txkodebarang.text.toString(),lis)
        val request: RequestQueue = Volley.newRequestQueue(this)
        request.add(send)
    }

    //function ketika tombol image ditekan akan memanggil function showpicturedialog
    fun image(view: View){
        takePhotoFromCamera()
    }

    private fun permissioncamera(){
        val permisison2= ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
        if(permisison2!= PackageManager.PERMISSION_GRANTED){
            makeRequest2()
        }
    }

    private  fun  makeRequest2(){
        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CAMERA),CAMERA_REQUEST_CODE)
    }

    fun saveImage(myBitmap: Bitmap):String {
        val bytes = ByteArrayOutputStream()
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
        val wallpaperDirectory = File(
                (Environment.getExternalStorageDirectory()).toString() + IMAGE_DIRECTORY)

        Log.d("fee",wallpaperDirectory.toString())
        if (!wallpaperDirectory.exists())
        {

            wallpaperDirectory.mkdirs()
        }

        try
        {
            Log.d("heel",wallpaperDirectory.toString())
            val f = File(wallpaperDirectory, ((Calendar.getInstance()
                    .getTimeInMillis()).toString()+" "+txnamapinjam.text.toString() + ".jpg"))
            f.createNewFile()
            val fo = FileOutputStream(f)
            fo.write(bytes.toByteArray())
            MediaScannerConnection.scanFile(this,
                    arrayOf(f.getPath()),
                    arrayOf("image/jpeg"), null)
            fo.close()
            Log.d("TAG", "File Saved::--->" + f.getAbsolutePath())

            return f.getAbsolutePath()
        }
        catch (e1: IOException) {
            e1.printStackTrace()
        }

        return ""
    }

    companion object {
        private val IMAGE_DIRECTORY = "/pinjambarang"
    }


    class Send : StringRequest {
        companion object{
            val Url= "http://192.168.43.81/pinjambarang/insert.php"

        }
        private var map:MutableMap<String,String> = HashMap()
        constructor(id_pinjam:String,nama_pinjam:String,no_hp:String,ktp:String,tgl_pinjam:String,kode_barang:String,Listener: Response.Listener<String>)
                :super(Request.Method.POST,Url,Listener,null){
            map.put("id_pinjam",id_pinjam)
            map.put("nama_pinjam",nama_pinjam)
            map.put("no_hp",no_hp)
            map.put("ktp",ktp)
            map.put("tgl_pinjam",tgl_pinjam)
            map.put("kode_barang",kode_barang)
        }

        override fun getParams(): MutableMap<String, String> {
            return map
        }
    }



    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val intentResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
        if (intentResult != null) {
            // If QRCode has no data.
            if (intentResult.contents == null) {
                Toast.makeText(this, "Scan Gagal", Toast.LENGTH_LONG).show()
            } else {
                //Jika QRCode berisi data.
                try {
                    // mendapatkan object dari hasil scan yang berupa data json
                    val obj = JSONObject(intentResult.contents)

                    // menampilkan value di interface
                    txkodebarang.setText( obj.getString("kode_barang"))
                    txnamabarang.setText(obj.getString("nama_barang"))

                } catch (e: JSONException) {
                    e.printStackTrace()
                    //Data tidak dalam format yang diharapkan. objek  seluruhnya akan ditampilkan dalam pesan toast.
                    Toast.makeText(this, intentResult.contents, Toast.LENGTH_LONG).show()
                }

            }
        }   else if(requestCode == CAMERA){
            try {
                val thumbnail = data!!.extras!!.get("data") as Bitmap
                imvktp!!.setImageBitmap(thumbnail)
                saveImage(thumbnail)
                Toast.makeText(this@MainActivity, "Ktp Tersimpan!", Toast.LENGTH_SHORT).show()
            }catch (rx:Exception){
                Toast.makeText(this@MainActivity, "Gagal!", Toast.LENGTH_SHORT).show()
            }
        }    else  if (requestCode == GALERY)
        {
            if (data != null)
            {
                val contentURI = data!!.data
                try
                {
                    val bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, contentURI)
                    val path = saveImage(bitmap)
                    Toast.makeText(this@MainActivity, "Ktp Tersimpan!", Toast.LENGTH_SHORT).show()
                    imvktp!!.setImageBitmap(bitmap)

                }
                catch (e: IOException) {
                    e.printStackTrace()
                    Toast.makeText(this@MainActivity, "Gagal!", Toast.LENGTH_SHORT).show()
                }

            }

        }
    }



    fun choosePhotoFromGallary() {
        val galleryIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(galleryIntent, GALERY)
    }

    private fun takePhotoFromCamera() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(intent, CAMERA)
    }

    private fun showPictureDialog() {
        val pictureDialog = AlertDialog.Builder(this)
        pictureDialog.setTitle("Select Action")
        val pictureDialogItems = arrayOf("Select photo from gallery", "Capture photo from camera")
        pictureDialog.setItems(pictureDialogItems
        ) { dialog, which ->
            when (which) {
                //0 -> choosePhotoFromGallary()
                1 -> takePhotoFromCamera()
            }
        }
        pictureDialog.show()
    }




    //function untuk membuat kode acak transaksi
    fun random(  length: Int): String {
        val sb = StringBuilder(length)
        val alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        val rardnd = Random()
        for ( i in 0 until sb.capacity() ) {
            val index = rardnd.nextInt( alphabet.length )
            sb.append( alphabet[ index ] )
        }
        id_pinjam.text = sb.toString()
        return sb.toString()

    }


    var dateChangeDialog = DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
        txtglpinjam.text = "$dayOfMonth-${month}-$year"
        tahun = year
        bulan = month
        hari = dayOfMonth
    }


    override fun onCreateDialog(id: Int): Dialog {
        when(id){
            20 -> return DatePickerDialog(this,dateChangeDialog,tahun,bulan,hari)
        }
        return super.onCreateDialog(id)
    }


}